﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Otus.Teaching.Ddd.CreditApplicationRegistration.Core.Domain.Dto;
using Otus.Teaching.Ddd.CreditApplicationRegistration.Core.Domain.Services;

namespace Otus.Teaching.Ddd.Cbs.CreditApplicationAggregator.Host.WebApi.Controllers
{
    /// <summary>
    /// API кредитных заявок
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class CreditApplicationsController : ControllerBase
    {
        private readonly ICreditApplicationService _creditApplicationService;

        public CreditApplicationsController(ICreditApplicationService creditApplicationService)
        {
            _creditApplicationService = creditApplicationService;
        }

        /// <summary>
        /// Получить список кредитных заявок
        /// </summary>
        [HttpGet("list")]
        [ProducesResponseType(typeof(CreditApplicationsForListDto),StatusCodes.Status200OK)]
        public async Task<IActionResult> GetCustomers()
        {
            return Ok(await _creditApplicationService.GetCreditApplicationsForListAsync());
        }
        
        /// <summary>
        /// Получить кредитную заявку по Id
        /// </summary>
        [HttpGet("{id:int}")]
        [ProducesResponseType(typeof(CreditApplicationDto), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetCustomer(Guid id)
        {
            return Ok(await _creditApplicationService.GetCreditApplicationAsync(id));
        }
        
        /// <summary>
        /// Создать новую заявку
        /// </summary>
        [HttpPost("create")]
        [ProducesResponseType(typeof(CreditApplicationDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ModelStateDictionary), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> CreateCreditApplication(CreateCreditApplicationDto dto)
        {
            var outputDto = await _creditApplicationService.CreateCreditApplicationAsync(dto);
            return Ok(outputDto);
        }       
    }
}
