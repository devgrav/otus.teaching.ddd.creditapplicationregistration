using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MassTransit;
using MassTransit.AspNetCoreIntegration;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NSwag;
using Otus.Teaching.Ddd.Contract;
using Otus.Teaching.Ddd.CreditApplicationRegistration.Application.Services;
using Otus.Teaching.Ddd.CreditApplicationRegistration.Core.Domain.Gateways;
using Otus.Teaching.Ddd.CreditApplicationRegistration.Core.Domain.Repositories;
using Otus.Teaching.Ddd.CreditApplicationRegistration.Core.Domain.Services;
using Otus.Teaching.Ddd.CreditApplicationRegistration.DataAccess.Repositories;
using Otus.Teaching.Ddd.CreditApplicationRegistration.Integration.Producers;

namespace Otus.Teaching.Ddd.Cbs.CreditApplicationAggregator.Host
{
    public class Startup
    {
        public ILoggerFactory LoggerFactory { get; }
        public IConfiguration Configuration { get; }
        
        public Startup(IConfiguration configuration, ILoggerFactory loggerFactory)
        {
            LoggerFactory = loggerFactory;
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddOpenApiDocument(options =>
            {
                //Title in header of api
                options.Title = "API Doc";
                //Version in header of api
                options.Version = "1.0";
                options.AddSecurity("JWT", Enumerable.Empty<string>(), new OpenApiSecurityScheme
                {
                    Type = OpenApiSecuritySchemeType.ApiKey,
                    Name = "Authorization",
                    In = OpenApiSecurityApiKeyLocation.Header,
                    Description = "Please insert JWT with Bearer into field"
                });
            });
            
            services.AddScoped<ICreditApplicationPipelineGateway, CreditApplicationPipelineGateway>();
            services.AddScoped<ICreditApplicationService, CreditApplicationServiceWithDomain>();            
            services.AddScoped<ICreditApplicationRepository, FakeCreditApplicationRepository>();
            
            services.Configure<IntegrationEndpointSettings>(Configuration.GetSection("IntegrationEndpoints"));           
            
            services.AddMassTransit(ConfigureBus(), LoggerFactory);
        }
        
        IBusControl ConfigureBus() => Bus.Factory.CreateUsingRabbitMq(cfg =>
        {
            var integrationSettings = new IntegrationEndpointSettings();
            Configuration.GetSection("IntegrationEndpoints").Bind(integrationSettings);
            
            cfg.Host(integrationSettings.MessageBrokerEndpoint);

            EndpointConvention.Map<CreateNewCreditApplicationContract>(new Uri($"{integrationSettings.MessageBrokerEndpoint}/createNewCreditApplication"));
        });

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            
            app.UseOpenApi();
            app.UseSwaggerUi3();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}