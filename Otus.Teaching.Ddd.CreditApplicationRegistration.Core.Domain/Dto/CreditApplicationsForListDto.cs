﻿using System.Collections.Generic;

namespace Otus.Teaching.Ddd.CreditApplicationRegistration.Core.Domain.Dto
{
    public class CreditApplicationsForListDto
    {
        public List<CreditApplicationsForListItemDto> Items { get; set; }
    }
}
