﻿using System;
using Otus.Teaching.Ddd.CreditApplicationRegistration.Core.Domain.Aggregates.CreditApplicationAgg;

namespace Otus.Teaching.Ddd.CreditApplicationRegistration.Core.Domain.Dto
{
    ///<summary>
    /// Данные создания заявки
    /// </summary>
    /// <example>
    ///{
    ///    "customerId": "1fb6f3ef-fd58-4df9-bb29-0bc6f9b57241",
    ///    "channel": 2,
    ///    "Sum": 10000.12
    ///}
    /// </example>
    public class CreateCreditApplicationDto
    {        
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }
        public AcquisitionChannel Channel { get; set; }
        public decimal Sum { get; set; }
        public int DesiredRate { get; set; }
        public DateTime Term { get; set; }

        public string AuthorFirstName { get; set; }

        public string AuthorLastName { get; set; }

        public string AuthorLogin { get; set; }

        public string AuthorEmail { get; set; }
    }
}
